package com.example.demomodjk;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InErrorController {

    @GetMapping("/inerrorpage/{code}")
    public ResponseEntity<String> inerrorpage(@PathVariable int code) {
        return new ResponseEntity<>(HttpStatus.valueOf(code));
    }
}