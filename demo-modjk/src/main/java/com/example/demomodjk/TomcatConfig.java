package com.example.demomodjk;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TomcatConfig {

    private static final String PROTOCOL = "AJP/1.3";

    @Value("${tomcat.ajp.port:8090}")
    private int ajpPort;
    @Value("${tomcat.ajp.packetsize:4096}")
    private int packetSize;
    @Value("${tomcat.ajp.maxthreads:50}")
    private int maxThreads;

    @Bean
    public TomcatServletWebServerFactory tomcatWebServerFactory() {
        TomcatServletWebServerFactory webServerFactory = new TomcatServletWebServerFactory();
        webServerFactory.addAdditionalTomcatConnectors(createConnectorAJP());
        return webServerFactory;
    }

    private Connector createConnectorAJP() {
        Connector connector = new Connector("AJP/1.3");
        connector.setProperty("packetSize", String.valueOf(packetSize));
        connector.setProperty("maxThreads", String.valueOf(maxThreads));

        connector.setPort(ajpPort);

        return connector;
    }



}