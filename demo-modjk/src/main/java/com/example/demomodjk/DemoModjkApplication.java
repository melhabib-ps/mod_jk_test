package com.example.demomodjk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoModjkApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoModjkApplication.class, args);
	}

}
