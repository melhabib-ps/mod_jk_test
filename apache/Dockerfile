FROM debian:jessie-20190812

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
#RUN groupadd -r www-data && useradd -r --create-home -g www-data www-data

ENV HTTPD_PREFIX /usr/local/apache2
ENV PATH $PATH:$HTTPD_PREFIX/bin
ENV MODJK_VERSION 1.2.41
ENV MODJK_URL http://archive.apache.org/dist/tomcat/tomcat-connectors/jk/tomcat-connectors-$MODJK_VERSION-src.tar.gz
ENV HTTPD_VERSION 2.2.21
ENV HTTPD_BZ2_URL http://archive.apache.org/dist/httpd/httpd-$HTTPD_VERSION.tar.bz2
ENV HTTP_SRC /src/httpd
ENV MOD_JK_SRC /src/mod_jk


RUN mkdir -p "$HTTPD_PREFIX" \
	&& chown www-data:www-data "$HTTPD_PREFIX"
WORKDIR $HTTPD_PREFIX

# install httpd runtime dependencies
# https://httpd.apache.org/docs/2.4/install.html#requirements
RUN set -x \
    && apt-get update \
	&& apt-get install -y --no-install-recommends \
		libapr1 \
		libaprutil1 \
		libpcre++0 \
		libssl1.0.0 \
# see https://httpd.apache.org/download.cgi#verify
    && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B1B96F45DFBDCCF974019235193F180AB55D9977 \
	# && rm -r /var/lib/apt/lists/*
    && buildDeps=' \
		autoconf \
        ca-certificates \
		curl \
		bzip2 \
		gcc \
		libapr1-dev \
		libaprutil1-dev \
		libc6-dev \
		libpcre++-dev \
		libssl-dev \
        libtool-bin \
		make \
	' \
	&& apt-get install -y --no-install-recommends $buildDeps \
	&& curl -SL "$HTTPD_BZ2_URL" -o httpd.tar.bz2 \
	&& curl -SL "$HTTPD_BZ2_URL.asc" -o httpd.tar.bz2.asc \
	&& gpg --verify httpd.tar.bz2.asc \
	&& mkdir -p ${HTTP_SRC} \
	&& tar -xvf httpd.tar.bz2 -C ${HTTP_SRC} --strip-components=1 \
	&& rm httpd.tar.bz2* \
	&& cd ${HTTP_SRC} \
	&& ./configure --enable-so --enable-ssl --prefix=$HTTPD_PREFIX \
	&& make -j"$(nproc)" \
	&& make install \
	&& sed -ri ' \
		s!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g; \
		s!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g; \
		' /usr/local/apache2/conf/httpd.conf \
    # install mod_jk
    && curl -o mod_jk.tar.gz "$MODJK_URL" \
    && mkdir -p ${MOD_JK_SRC} \
    && tar -xvf mod_jk.tar.gz -C ${MOD_JK_SRC} --strip-components=1 \
    && rm mod_jk.tar.gz* \
    && cd ${MOD_JK_SRC}/native \
    && ./buildconf.sh \
    && ./configure --with-apxs \
    && make \
    && cp apache-2.0/mod_jk.so /usr/local/apache2/modules/mod_jk.so \
    # clean
    && pwd \
    && rm -r /var/lib/apt/lists/* \
	&& rm -r /src \
	&& apt-get purge -y --auto-remove $buildDeps \
    && echo "Include /usr/local/apache2/conf/mod_jk.conf" >> /usr/local/apache2/conf/httpd.conf

COPY httpd-foreground /usr/local/bin/
COPY workers.properties /usr/local/apache2/conf/workers.properties
COPY mod_jk.conf /usr/local/apache2/conf/mod_jk.conf

EXPOSE 80
CMD ["httpd-foreground"]
